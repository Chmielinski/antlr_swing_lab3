grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog 
    : (stat | block)+ EOF!
    ;

block 
    : BEGIN^ NL! (stat | block)* END!
    ;

stat
    : expr NL -> expr
    | VAR ID (ASSIGN expr)? NL -> ^(VAR ID) ^(ASSIGN ID expr)?
    | ID ASSIGN expr NL -> ^(ASSIGN ID expr)
    | if_stat NL -> if_stat
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
    
if_expr
    : expr
      ( EQ^ expr
      | NEQ^ expr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

if_stat
	  : IF^ if_expr THEN! NL? block (ELSE! NL? block)?
	  ;

  
VAR 
    : 'var'
    ;

IF
	  : 'if'
	  ;
    
ELSE
	  : 'else'
	  ;
  
THEN
	  : 'then'
	  ;
  
BEGIN 
	  : '{'
	  ; 
  
END
	  : '}'
	  ;
  
EQ
	  : '=='
	  ;
  
NEQ
	  : '!='
	  ;
  
ID 
    : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT 
    : '0'..'9'+
    ;

NL 
    : '\r'? '\n' 
    ;

WS 
    : (' ' | '\t')+ {$channel = HIDDEN;} 
    ;

LP
    : '('
    ;

RP
    : ')'
    ;

ASSIGN
    : '='
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MUL
    : '*'
    ;

DIV
    : '/'
    ;
  