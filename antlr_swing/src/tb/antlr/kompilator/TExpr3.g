tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
  Integer conditionsCount = 0;
}
prog      : (e+=codeblock | e+=expr | e+=comp | d+=decl)* -> program(name={$e},declarations={$d})
;

codeblock : ^(BEGIN {enterScope();} (e+=codeblock | e+=expr | e+=comp | d+=decl)* {leaveScope();}) -> block(statement={$e},declaration={$d})
;

decl      : ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declare(p1={$ID.text})
;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr      : ^(PLUS  e1=expr e2=expr) -> plus(p1={$e1.st},p2={$e2.st})
	        | ^(MINUS e1=expr e2=expr) -> minus(p1={$e1.st},p2={$e2.st})
	        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
	        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
	        | ^(ASSIGN i1=ID   e2=expr) {locals.hasSymbol($i1.text);} -> assign(p1={$ID},p2={$e2.st})
	        | INT                      -> int(i={$INT.text})
	        | ID                       {locals.checkSymbol($ID.text);} -> id(i={$ID.text})
	        | ^(IF if_processed=comp then_block=codeblock else_block=codeblock?) {conditionsCount++;} -> if(if_processed={$if_processed.st},then_block={$then_block.st},else_block={$else_block.st},nr={conditionsCount.toString()})
;        

comp      :  ^(EQ    e1=expr e2=expr) -> isEqual(p1={$e1.st},p2={$e2.st})
	        | ^(NEQ    e1=expr e2=expr) -> isNotEqual(p1={$e1.st},p2={$e2.st},nr={conditionsCount.toString()})
;
    